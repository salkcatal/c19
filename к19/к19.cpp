﻿#include <iostream>

using namespace std;
 class Animal
{
public:
	virtual void voice() { cout << "Voices of animals \n"; };
	virtual ~Animal() { cout<<"delete \n"; };
};

class Dog: public Animal
{
public:
	void voice() override { cout << "Woof  " ; };
	~Dog() { cout << "delete \n"; };
};

class Cat : public Animal
{
public:
	void voice() override { cout << "Miay  "; };
	~Cat() { cout << "delete \n"; };
};

class Cow : public Animal
{
public:
	void voice() override { cout << "Muuu  "; };
	~Cow() { cout << "delete \n"; };
};

int main()
{
	Animal* cat = new Cat();
	Animal* dog = new Dog();
	Animal* cow = new Cow();
	Animal* animal = new Animal();
	animal->voice();
	Animal* p[7] = { dog, cow, cat, dog, dog, cat, cow };
	for (int i = 0; i < 7; i++)
		p[i]->voice();
	delete cat;
	delete dog;
	delete cow;
	delete animal;
}